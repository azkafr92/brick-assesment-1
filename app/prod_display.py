import json

from bs4 import BeautifulSoup

from .helper import get_html_text

BASE_URL = "https://www.tokopedia.com/p/handphone-tablet/handphone"
HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "en-US,en;q=0.5",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive",
    "Cookie": '_srp_sf_=false; _abck=35C50ED444F8F3EC24EBBEB3A96AC58F~0~YAAQfQA9F+MZW918AQAAozZj7wbam7trST0TPUhsLg/WLb7v70mHUTPphsOc5bAgsncnYI+iAS/1FGms6C4pDMz+eSjk/yIGffCDwYIz3tmnylbwFQSegD/Ir3QjT/IbgjixlwgNmQKyO8J3VrU8QvYNFMXQCrHnRzGaI7bMuidsRKe+FBhi7JP88FX++dAuPhUH0junHnkvSXexX7YnOeFDdCLrDUfLkAPL9g6Wvvsz2lxKLzIyTfx23SdthjYQzghaLmmzCfRNt4Y/OTCWZ++3guJYXgj2O8luet8EsnYSB3Y4QE7sIMez7kjEmGP80yHV/HjPHAyh1WnozzLxnwsaquRdOzocyQrnTtqo+MBZjStdDCROimoeNvHpynFfOnzX8MVa3b4+phZOaYfjPUjXYl/uSTHiYg9f~-1~-1~-1; ISID=%7B%22www.tokopedia.com%22%3A%22d3d3LnRva29wZWRpYS5jb20%3D.b796a820d3e91457597e158ac45d5aa4.1636108511676.1636105671792.1636110519398.6%22%7D; __auc=9979a3bc17637aa131ad960bcbe; _ga=GA1.2.2083477945.1607251662; _UUID_NONLOGIN_=8078f64c86e9c3a05ae76dc2e3f9cb5a; DID=13e048ccb0a6c3bd4602eb4863fdccd3bdc4434620e5cd257ae26ff496cd6e1877ede2c0493a8a1fb2026b243ed6f059; DID_JS=MTNlMDQ4Y2NiMGE2YzNiZDQ2MDJlYjQ4NjNmZGNjZDNiZGM0NDM0NjIwZTVjZDI1N2FlMjZmZjQ5NmNkNmUxODc3ZWRlMmMwNDkzYThhMWZiMjAyNmIyNDNlZDZmMDU547DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=; g_state={"i_l":1,"i_p":1622735038539}; hfv_banner=true; _hjid=47dca1fe-5d9c-4101-a0d2-fd2c90e13bd3; dt_intl=JT_7DakzPgwEX7hggZO9hVNEdvi65HTEpT_SKXZ05h8Idr; _ga_70947XW48P=GS1.1.1636108510.47.1.1636110515.53; zarget_visitor_info=%7B%7D; zarget_user_id=1d319d84-3925-4a5a-9188-3789b31e0630; cto_bundle=95-fpl9jWkg2TzBjUWhwT1oxOUdaMCUyRlVSRTlWWFdOSjYlMkJHWVl1blhUQ2dFJTJGVDhlamk5WlBTTFgxR3VmUjdmZkZmY3hremNIVmhTVTZwQ3ljUzhsUndES0IlMkJTJTJCSTZ2SVFBYVhvWHNzZTM4Mkl0JTJCSllyM0Z1NlMyJTJCUUdsMDUzQU0lMkJvSVFlUDRtbGZTbW0yZks5Q1pqbmRxSzdBJTNEJTNE; _SID_Tokopedia_=wgrCpVNs8ZEHvDY0gnFF9VLfqfxEjNuaFM57OfHfG9mVedcR0iJRzsuiJxWFKyfig8q0SsP_pgYhVdqeXPlZOEZfRrlVxHlgvRv42EwTcu0T5RHICRHePsON4A-anASs; G_ENABLED_IDPS=google; l=1; aus=1; _CAS_=%7B%22dId%22%3A2202%2C%22aId%22%3A8593879%2C%22lbl%22%3A%22Alamat%20Rumah%20Azka%20Fadhli%20Ramadhan%22%2C%22cId%22%3A167%2C%22long%22%3A%22107.005405%22%2C%22lat%22%3A%22-6.194972%22%2C%22pCo%22%3A%22%22%7D; user-preference=true; _gcl_au=1.1.645166163.1632398531; TOPATK=bx2YI2HgRniEwMEx8_-GVA; TOPRTK=3E5ypqOOSMKTN0Kya5ELaA; _gid=GA1.2.1428585153.1636032672; _fbp=fb.1.1636033241049.1670080845; bm_sz=EDCC804A860697EE33B1069B642A8ECC~YAAQzAA9F4IN7el8AQAA0yLJ7w3zXGtZ69KyAl2jeeyT74EHUkpXJIhBJYGSl69no6k3HC+NX9EHEKCTR51e5leJmApG/MOZeAEckKxBcnrWs8pLj8DkOpjmGq267f+8wnUynBrhBZqmfipAxr6IJXvv91uhQ56Q6EaMbDBWyR2Iis7zOce84Ot/GjYI1Yo2ty6N9Rw41ldNCgy2XfJAxhC4Tyj7Hjl4TmO0lPtwdT4gKWmALuWr2Yr5qDndCGLfETWdP3sVoCWu2wi7vk3SayhqUxfkCDLAuWQYI5vMJG7JFCOWUM2gQTMBVIWqlNAnmvj+OJAmPbXi6+3bq3E=~4601157~4535347; tuid=7349037',
    "Host": "www.tokopedia.com",
    "Sec-Fetch-Dest": "document",
    "Sec-Fetch-Mode": "navigate",
    "Sec-Fetch-Site": "none",
    "Sec-Fetch-User": "?1",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0",
}


def save_prod_display(html_soup: BeautifulSoup, file_name: str):
    with open(file_name, "w") as f:
        json.dump(
            json.loads(
                [
                    item.strip()[15:].replace('\\"', "'")
                    for item in (
                        html_soup.find_all("script", attrs={"type": "text/javascript"})[
                            3
                        ].string.split(";\n")
                    )
                    if item.strip().startswith("window.__cache")
                ][0]
            ),
            f,
        )


def get_product_keys(data: dict) -> list:
    return [
        key
        for key in data.keys()
        if (
            (key.startswith("AceSearchProduct"))
            & (not ("label" in key))
            & (not ("badge" in key))
        )
    ]


def get_and_save_prod_display(file_name: str, url: str, params: dict):
    save_prod_display(
        html_soup=BeautifulSoup(
            get_html_text(url=url, params=params, headers=HEADERS),
            "html.parser",
        ),
        file_name=file_name,
    )


if __name__ == "__main__":
    use_requests = False
    start_page = 1
    finish_page = 2
    for page in range(start_page, finish_page + 1):
        file_name = f"prod_display_p{page}.json"

        if use_requests:
            get_and_save_prod_display(
                file_name=file_name, url=BASE_URL, params={"ob": 5, "page": page}
            )

        with open(file_name, "r") as f:
            data = json.load(f)

        product_keys = set(get_product_keys(data=data))
        product_urls = [data.get(key) for key in product_keys]
