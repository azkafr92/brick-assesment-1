import requests
import traceback


def get_html_text(url: str, params: dict = None, headers: dict = None) -> str:
    result = ""
    try:
        resp = requests.get(url, params=params, headers=headers)
        if resp.status_code == 200:
            result = resp.text
    except:
        traceback.print_exc()
    finally:
        return result
