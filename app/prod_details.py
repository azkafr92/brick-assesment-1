from bs4 import BeautifulSoup

from .helper import get_html_text

HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "en-US,en;q=0.5",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive",
    "DNT": "1",
    "Host": "www.tokopedia.com",
    "Pragma": "no-cache",
    "Sec-Fetch-Dest": "document",
    "Sec-Fetch-Mode": "navigate",
    "Sec-Fetch-Site": "none",
    "Sec-Fetch-User": "?1",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0",
}


def _prod_img_data(prod_display_data: dict) -> str:
    imgs = [prod_display_data.get("image_url"), prod_display_data.get("image_url_700")]
    if any(imgs):
        return imgs[-1]
    return ""


def _prod_desc(page_source: BeautifulSoup) -> str:
    a = "; ".join(
        [
            item.text
            for item in page_source.find(
                "ul", attrs={"data-testid": "lblPDPInfoProduk"}
            ).find_all("li")
            if bool(item)
        ]
    )
    b = page_source.find("div", attrs={"data-testid": "lblPDPDescriptionProduk"})
    b = b.get_text("; ") if b else ""

    return f"{a}; {b}"


def _prod_store(prod_display_data: dict) -> str:
    store = ""
    shop = prod_display_data.get("shop")
    if isinstance(shop, dict):
        store = shop.get("id")

    return store


def _get_prod_details(prod_display_data: dict, page_source: BeautifulSoup) -> dict:
    return {
        "name": prod_display_data.get("name"),
        "desc": _prod_desc(page_source=page_source),
        "img": _prod_img_data(prod_display_data),
        "price": prod_display_data.get("price"),
        "original_price": prod_display_data.get("original_price"),
        "rating": prod_display_data.get("rating"),
        "store": _prod_store(prod_display_data),
    }


def get_prod_details(prod_display_data: dict):
    return _get_prod_details(
        prod_display_data=prod_display_data,
        page_source=BeautifulSoup(
            get_html_text(url=prod_display_data["url"], headers=HEADERS),
            "html.parser",
        ),
    )


if __name__ == "__main__":
    example_prod_display = {
        "id": 1883905997,
        "url": "https://www.tokopedia.com/xiaomi/xiaomi-redmi-note-10s-8-128g-nfc-layar-6-43-super-amoled-smart-hp-onyx-gray?whid=0",
        "image_url": "https://images.tokopedia.net/img/cache/200-square/VqbcmM/2021/8/27/ffc04108-95c6-4e6d-a405-7381a04d2a41.jpg",
        "image_url_700": "https://images.tokopedia.net/img/cache/700/VqbcmM/2021/8/27/ffc04108-95c6-4e6d-a405-7381a04d2a41.jpg",
        "category_id": 65,
        "ga_key": "/category/handphone-tablet/handphone/xiaomi/xiaomi-redmi-note-10s-8-128g-nfc-layar-6-43-super-amoled-smart-hp-onyx-gray",
        "count_review": 1634,
        "discount_percentage": 6,
        "is_preorder": False,
        "name": "Xiaomi Redmi Note 10S 8/128G NFC Layar 6.43” Super AMOLED Smart HP",
        "price": "Rp3.099.000",
        "original_price": "Rp3.299.000",
        "rating": 5,
        "wishlist": False,
        "labels": [
            {
                "type": "id",
                "generated": True,
                "id": "AceSearchProduct1883905997.labels.0",
                "typename": "AceSearchLabel",
            }
        ],
        "badges": [
            {
                "type": "id",
                "generated": True,
                "id": "AceSearchProduct1883905997.badges.0",
                "typename": "AceSearchBadge",
            }
        ],
        "shop": {
            "type": "id",
            "generated": False,
            "id": "AceShop8043004",
            "typename": "AceShop",
        },
        "label_groups": [
            {
                "type": "id",
                "generated": True,
                "id": "AceSearchProduct1883905997.label_groups.0",
                "typename": "AceSearchLabelUnify",
            },
            {
                "type": "id",
                "generated": True,
                "id": "AceSearchProduct1883905997.label_groups.1",
                "typename": "AceSearchLabelUnify",
            },
            {
                "type": "id",
                "generated": True,
                "id": "AceSearchProduct1883905997.label_groups.2",
                "typename": "AceSearchLabelUnify",
            },
            {
                "type": "id",
                "generated": True,
                "id": "AceSearchProduct1883905997.label_groups.3",
                "typename": "AceSearchLabelUnify",
            },
        ],
        "__typename": "AceSearchProduct",
    }
    example_prod_details_page_source = ...

    prod_details = get_prod_details(prod_display_data=example_prod_display)
    print(prod_details)
