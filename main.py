import traceback
import csv
import os
from app.prod_display import BASE_URL, get_and_save_prod_display, get_product_keys, json
from app.prod_details import get_prod_details
from icecream import ic

field = ["name", "desc", "img", "price", "original_price", "rating", "store"]
if not (os.path.exists("products.csv")):
    os.mknod("products.csv")
    with open("products.csv", "w") as f:
        writer = csv.DictWriter(
            f,
            fieldnames=field,
        )
        writer.writeheader()

use_requests = True
start_page = 1
finish_page = 2
for page in range(start_page, finish_page + 1):
    file_name = f"prod_display_p{page}.json"

    if use_requests:
        get_and_save_prod_display(
            file_name=file_name, url=BASE_URL, params={"ob": 5, "page": page}
        )

    with open(file_name, "r") as f:
        data = json.load(f)

    product_keys = set(get_product_keys(data=data))
    products_display_data = [data.get(key) for key in product_keys]
    for index, product in enumerate(products_display_data):
        status = f"page {page}, product {index}"
        ic(status)
        try:
            prod_details = get_prod_details(prod_display_data=product)
            with open("products.csv", "a") as f:
                writer = csv.DictWriter(f, fieldnames=field)
                writer.writerow(prod_details)
        except:
            traceback.print_exc()
